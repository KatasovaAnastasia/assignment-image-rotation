#ifndef IMAGE_H
#define IMAGE_H

#include  <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

bool create_img(struct image* img, uint64_t width, uint64_t height);
void free_img(struct image* img);

#endif
