#ifndef ROTATION
#define ROTATION

#include "image.h"

struct image rotate( struct image const source );

#endif  
