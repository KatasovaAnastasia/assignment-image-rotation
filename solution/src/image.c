#include "image.h"

static void create_img_size(struct image* img, uint64_t width, uint64_t height);
static bool create_img_data(struct image* img);

bool create_img(struct image* img, uint64_t width, uint64_t height){
    create_img_size(img, width, height);
    return create_img_data(img);
}

void free_img(struct image* img){
    free(img->data);
}

static void create_img_size(struct image* img, uint64_t width, uint64_t height){
    img->width = width;
    img->height = height;
}

static bool create_img_data(struct image* img){
    img->data = malloc(sizeof(struct pixel)*img->width*img->height);
    if (img->data)
        return true;
    return false;
}
