#include "rotation.h"

struct image rotate( struct image const source ){
    const uint64_t w = source.width;
    const uint64_t h = source.height;

    struct image out_img = {0};

    create_img(&out_img, h, w);

    if(source.data){
        for (uint64_t i = 0; i < w; i++) {
            for (uint64_t j = 0; j < h; j++) {
                out_img.data[i * out_img.width + j] =  source.data[(h-1-j) * w + i];
            }
        }
    }
    

    return out_img;
}
