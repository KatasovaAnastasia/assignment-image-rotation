#include "main.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if(argc!=3) 
        return -1;
        
    struct image in_img;
    struct image out_img;

    FILE *in_f = fopen(argv[1], "rb");
    FILE *out_f = fopen(argv[2], "wb");

    if(from_bmp(in_f, &in_img) != READ_OK)
        return -1;

    out_img = rotate(in_img);

    if(to_bmp(out_f, &out_img) != WRITE_OK)
        return -1;

    fclose(in_f);
    fclose(out_f);

    free_img(&in_img);
    free_img(&out_img);

    return 0;
    
}
