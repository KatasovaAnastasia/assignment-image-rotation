#include "bmp.h"

static enum read_status take_data(struct image* img, FILE* in);
static enum write_status send_data(struct image const* img, FILE* out);
static struct bmp_header new_header(struct image const* img);

enum read_status from_bmp( FILE* in, struct image* img ){
    enum read_status err_read;
    struct bmp_header* header = malloc(sizeof(struct bmp_header)); 
    if(header == NULL) 
        return READ_ERROR_MEMORY;
    
    fread(header, sizeof(struct bmp_header), 1, in);
    if (header->bfType > BF_TYPE || header->bfType <= 0) 
        return READ_INVALID_SIGNATURE;
    if (header->biBitCount != BI_BIT)
        return READ_INVALID_BITS;
    if (header->biSize != BI_SIZE)
        return READ_INVALID_HEADER;
    
    if(!create_img(img, header->biWidth, header->biHeight))
        return READ_ERROR_MEMORY;
    free(header);
    err_read = take_data(img, in);
    if(err_read != READ_OK)
        return err_read;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    enum write_status err_write_status;
    struct bmp_header header = new_header(img);
    size_t err_write;
    err_write = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (err_write != 1)
        return WRITE_ERROR;
    err_write_status = send_data(img, out);
    if(err_write_status != WRITE_OK)
        return err_write_status;
    return WRITE_OK;
}


static enum read_status take_data(struct image* img, FILE* in){
    const uint64_t w = img->width;
    const uint64_t h = img->height;
    size_t err_read;
    int err_seek;
    for(uint32_t i = 0; i < h; i++) {
         err_read = fread(&(img->data[i*w]), sizeof(struct pixel), w, in);
         if (err_read != w)
            return READ_INVALID_BITS;
         err_seek = fseek(in, (int64_t) w % 4, SEEK_CUR);
         if(err_seek != 0)
            return READ_INVALID_BITS;
    }
    return READ_OK;
}

static struct bmp_header new_header(struct image const* img){
    const uint64_t w = img->width;
    const uint64_t h = img->height;
    struct bmp_header header = {
        .bfType = HEADER_BF_TP,
        .bfileSize = (sizeof(struct bmp_header)
                      + h * w * sizeof(struct pixel)
                      + h * (w % 4)),
        .bfReserved = HEADER_DEFAULT,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = HEADER_BI_SIZE,
        .biWidth = w,
        .biHeight = h,
        .biPlanes = HEADER_BI_PL,
        .biBitCount = HEADER_BIT_C,
        .biCompression = HEADER_DEFAULT,
        .biSizeImage = h * w * sizeof(struct pixel) + (w % 4) * h,
        .biXPelsPerMeter = HEADER_DEFAULT,
        .biYPelsPerMeter = HEADER_DEFAULT,
        .biClrUsed = HEADER_DEFAULT,
        .biClrImportant = HEADER_DEFAULT
    };
    return header;
}

static enum write_status send_data(struct image const* img, FILE* out){
    const uint64_t w = img->width;
    const uint64_t h = img->height;
    size_t err_write;
    const size_t st = 0;
    for(uint32_t i = 0; i < h; i++) {
        err_write = fwrite(&(img->data[i*w]),sizeof(struct pixel), w, out);
        if(err_write != w)
            return WRITE_ERROR;
        err_write = fwrite(&st, 1, w % 4, out);
        if(err_write != w % 4)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}
